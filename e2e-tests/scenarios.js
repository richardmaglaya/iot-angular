'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('my app', function() {


  it('should automatically redirect to /view1 when location hash/fragment is empty', function() {
    browser.get('index.html');
    expect(browser.getLocationAbsUrl()).toMatch("/table");
  });


  describe('table', function() {

    beforeEach(function() {
      browser.get('index.html#!/table');
    });


    it('should render view1 when user navigates to /table', function() {
      expect(element.all(by.css('[ng-view] p')).first().getText()).
        toMatch(/partial for table/);
    });

  });


  describe('chart', function() {

    beforeEach(function() {
      browser.get('index.html#!/chart');
    });


    it('should render view2 when user navigates to /chart', function() {
      expect(element.all(by.css('[ng-view] p')).first().getText()).
        toMatch(/partial for chart/);
    });

  });
});
