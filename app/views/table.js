'use strict';

angular.module('myApp.table', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/table', {
    templateUrl: 'views/table.html',
    controller: 'TableCtrl'
  });
}])

.controller('TableCtrl', ['$scope', '$http', function($scope, $http) {
    var details = [];
  
    //-- Can be replaced by a call to single API that returns array of data
    for (var i=0; i<10; i++) {
        $http.get('http://resu.me/health_check').success(function(response) { details.push(response); });
    }
    $scope.details = details;
    
}]);