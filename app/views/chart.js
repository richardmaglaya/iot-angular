'use strict';

angular.module('myApp.chart', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/chart', {
    templateUrl: 'views/chart.html',
    controller: 'ChartCtrl'
  });
}])

.controller('ChartCtrl', [function() {
    
    var chart = c3.generate({
        bindto: '#chart',
        data: {
            columns: [
                ['data1', 30, 200, 100, 400, 150, 250],
                ['data2', 50, 20, 10, 40, 15, 25]
            ],
            axes: {
                data2: 'y2'
            },
            types: {
                data2: 'bar' // ADD
            }
        }
    });
    
}]);